<?php
namespace Rubeus\Util\EnvioEmail\Aplicacao;
use Rubeus\Servicos\Email\Email as EmailEscape;

class Email{
    private $mail;

    public function __construct($id){
        $this->mail = new EmailEscape($id);
    }

    public function alterarEmailEnvio($destino){
        return $this->mail->setDestino($destino);
    }

    public function setArquivo($arquivo){
        $this->mail->setArquivo($arquivo);
    }

    public function enviarEmail($endereco, $enderecoEmail, $array,$copiaOculta=false){
        ob_start();
        include DIR_BASE.'/'.$endereco;
        $texto = ob_get_contents();
        ob_end_clean();
        $substituir = $novoValor = [];
        if(file_exists(DIR_BASE.'/'.SUBSTITUICAO_PASIVA_EMAIL)){
            $substituicaoPassiva = json_decode(file_get_contents(DIR_BASE.'/'.SUBSTITUICAO_PASIVA_EMAIL),true);
            foreach ($substituicaoPassiva as $key=>$value){
                $substituir[] = $key;
                $novoValor[] = $value;
            }
        }

        $this->mail->setTexto(str_replace($substituir, $novoValor, $texto));
        $this->mail->setAssunto($array['assunto']);
        
        $this->mail->setDestino($enderecoEmail);

        if ($copiaOculta) {
            $this->mail->setCopiaOculta($copiaOculta);
        }
        
        if (isset($array['emailResposta'])) {
            $this->mail->setEmailResposta($array['emailResposta']);
        }
        return $this->mail->enviar();
    }

    public function getErro(){
        return $this->mail->getErroCodigo();
    }

}
