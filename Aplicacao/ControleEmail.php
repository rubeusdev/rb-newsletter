<?php
namespace Rubeus\Util\EnvioEmail\Aplicacao;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Util\EnvioEmail\Dominio\EnderecoEmail;
use Rubeus\Util\EnvioEmail\Dominio\PullEnvioEmail;
use Rubeus\Util\EnvioEmail\Dominio\EnvioEmail;
use Rubeus\Util\EnvioEmail\Dominio\EmailEntidade;

class ControleEmail{
    protected $mail;
    protected $enderecoEmail;
    protected $pullEnvioEmail;
    protected $envioEmail;
    protected $dadosEmail;
    protected $email;
    protected $situacaoEmail;
    
    public function __construct(){
        $this->dadosEmail = new ConfiguracaoEmail();
        $this->mail = new Email($this->dadosEmail->iniciarPadrao());
    }
    
    public function iniciarEnvio($email,$assunto){
        $this->enderecoEmail = new EnderecoEmail();
        $this->pullEnvioEmail = new PullEnvioEmail();
        $repositorio = Conteiner::getInstancia('Repositorio');
        
        $this->enderecoEmail->setEndereco($email);
        $repositorio->consultarId($this->enderecoEmail);
        
        if(!$repositorio->registrarPadrao($this->enderecoEmail))return false;
        
        $this->email = new EmailEntidade();        
        $this->email->setAssunto($assunto);
        
        if(!$repositorio->registrarPadrao($this->email))return false;
        
        $this->pullEnvioEmail->setEnderecoEmail($this->enderecoEmail);
        return $repositorio->registrarPadrao($this->pullEnvioEmail);
    }
    
    public function finalizarEnvio(){
        $this->situacaoEmail = $this->mail->getErro();
        
        $repositorio = Conteiner::getInstancia('Repositorio');
        $this->envioEmail = new EnvioEmail();
        
        $this->envioEmail->setEmail($this->email);
        $this->envioEmail->setEnderecoEmail($this->enderecoEmail);
        $this->envioEmail->setPullEnvioEmail($this->pullEmail);
        $this->envioEmail->setSituacaoEnvioEmail($this->situa);
        
        if(!$repositorio->registrarPadrao($this->envioEmail)) return false;
        
        $this->pullEnvioEmail->setMomentoFim(date('Y-m-d H:i:s'));
        $this->pullEnvioEmail->setAtivo(0);
        return $repositorio->registrarPadrao($this->pullEnvioEmail);
    }
    
}