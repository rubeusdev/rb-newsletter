<?php
namespace Rubeus\Util\EnvioEmail\Aplicacao;   
use Rubeus\ContenerDependencia\Conteiner;

class ConfiguracaoEmail{
    private $dados;
    
    public function __construct(){
        $dados = Conteiner::get('EnvioEmailConfig');
        if($dados){ 
            $this->dados = $dados;
        }
    }
    
    public function getEmail($atributo){
        if($this->dados && $this->dados->$atributo){ 
            return $this->dados->$atributo;
        }
        return false;
    }

    public function iniciarPadrao(){
        if($this->dados){ 
            return $this->dados->emailEnvioPadrao;
        }
        return 1;
    }
   
}
